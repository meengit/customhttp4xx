<?php

namespace Drupal\customhttp4xx\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class CustomHTTP4xxController extends ControllerBase {

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * CustomHTTP4xxController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony\Component\HttpFoundation\RequestStack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $build = [
      '#type' => 'markup',
      '#markup' => $this->t("<div style=\"margin: 0 auto;text-align: center;width: 90 %;\"><p>Leiter konnte die angeforderte Seite nicht gefunden werden.</p><p>Unfortunately, the requested page could not be found.</p><p>Zur&uuml;ck zu/Go back to <a href=\"${host}\" alt=\"SanArena Home Site\">Home</a>.</p></div>"),
    ];
    return $build;
  }

}
