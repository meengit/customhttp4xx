<?php

namespace Drupal\customhttp4xx\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('system.404')) {
      $route->setDefaults([
        '_controller' => '\Drupal\customhttp4xx\Controller\CustomHTTP4xxController::content',
      ]);
    }
  }

}
