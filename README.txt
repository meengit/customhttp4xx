README.txt for Custom HTTP4xx Messages in Drupal 8
--------------------------------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Custom HTTP4xx Module is a tiny Drupal 8 Module to customize specific HTTP 4xx messages – e.g., to overwrite the message when an [HTTP status 404](https://httpstatuses.com/404) happened.

REQUIREMENTS
------------

No requirements.

RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

This module is currently not listed at [drupal.org](https://www.drupal.org/). I suggest an installation into the `modules/custom` directory using git clone:

```bash
git clone https://gitlab.com/meengit/admintoolbarpermissions.git
```

CONFIGURATION
-------------

No configuration, yust enable to use.

TROUBLESHOOTING
---------------

Nothing known.

FAQ
---

Nothing known.

MAINTAINERS
-----------

* Andreas, <https://gitlab.com/meengit/admintoolbarpermissions>

